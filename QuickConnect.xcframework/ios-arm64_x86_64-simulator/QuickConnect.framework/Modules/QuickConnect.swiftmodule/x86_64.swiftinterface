// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.2 (swiftlang-1200.0.45 clang-1200.0.32.28)
// swift-module-flags: -target x86_64-apple-ios11.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name QuickConnect
import AVFoundation
import CoreGraphics
import Foundation
import PLFanData
import PLFanDataCommon
import SocketIO
import Swift
import UIKit
public typealias QuickConnectModule = QuickConnect.GenericQuickConnectModule<QuickConnect.SocketIOProvider>
public class GenericQuickConnectModule<ConcreteSocketProvider> where ConcreteSocketProvider : QuickConnect.SocketProvider {
  public init(fontProvider: PLFanData.FontProvider)
  public func start(owner: UIKit.UIViewController)
  @objc public func close()
  @objc deinit
}
public protocol QuickConnectPresenter : AnyObject {
  func startListening(completion: @escaping PLFanData.WidgetCompletionHandler)
}
public protocol SocketClient {
  func on(event: Swift.String, callback: @escaping (([Any]) -> Swift.Void))
  func connect()
  func disconnect()
}
final public class SocketIOProvider : QuickConnect.SocketProvider {
  public init?(url: Foundation.URL)
  @objc deinit
  final public var socket: QuickConnect.SocketClient {
    get
  }
}
public protocol SocketProvider {
  init?(url: Foundation.URL)
  var socket: QuickConnect.SocketClient { get }
}
