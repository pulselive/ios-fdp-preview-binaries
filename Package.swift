// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "PLFanDataPreview",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "PLFanDataPreview",
            targets: [
                "QuickConnect",
                "SocketIO",
                "Starscream",
                "PLFanData-Dependencies"
            ]
        )
    ],
    dependencies: [
        .package(
            name: "PLFanDataPlatform",
            url: "https://bitbucket.org/pulselive/ios-fdp-binaries", .exact("2.6.8") // Don't break the line this is used by the publisdh regex script
        )
    ],
    targets: [
        .target(
            name: "PLFanData-Dependencies",
            dependencies: [
                "PLFanDataPlatform"
            ]
        ),
        .binaryTarget(
            name: "QuickConnect",
            path: "QuickConnect.xcframework"
        ),
        .binaryTarget(
            name: "SocketIO",
            path: "SocketIO.xcframework"
        ),
        .binaryTarget(
            name: "Starscream",
            path: "Starscream.xcframework"
        )
    ]
)
